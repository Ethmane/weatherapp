import React from 'react';
import { View, StatusBar } from 'react-native';
import Search  from './src/components/Search';
import Expo, { Font } from 'expo';
class App extends React.Component {
  
  state = {
    fontLoaded: false,
  }; 

  async componentDidMount() {
    await Font.loadAsync({
      'IndieFlower': require('./assets/fonts/IndieFlower.ttf'),
    });
    this.setState({ fontLoaded: true });
  }
  render() {
    return (
      
      <View style={{flex : 1}}>
        <StatusBar hidden = {true}/>
        { this.state.fontLoaded ? (
        <Search/>
         ) : null }  
      </View>
    
    );
  }
}

export default App;