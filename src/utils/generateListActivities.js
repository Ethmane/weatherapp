const activitesWithIcons = [
    {id : 0, name : '', sourceIcon: require('../../assets/icons/bicycle.png')},
    {id : 1, name : '', sourceIcon: require('../../assets/icons/coffee-cup.png')},
    {id : 2, name : '', sourceIcon: require('../../assets/icons/parc.png')},
    {id : 3, name : '', sourceIcon: require('../../assets/icons/cinema.png')},
    {id : 4, name : '', sourceIcon: require('../../assets/icons/conference.png')},
    {id : 5, name : '', sourceIcon: require('../../assets/icons/sauna.png')},
    {id : 6, name : '', sourceIcon: require('../../assets/icons/library.png')},
    {id : 7, name : '', sourceIcon: require('../../assets/icons/climb.png')},
    {id : 8, name : '', sourceIcon: require('../../assets/icons/beach.png')},
    {id : 9, name : '', sourceIcon: require('../../assets/icons/city-visit.png')},
    {id : 10, name : '', sourceIcon: require('../../assets/icons/ski.png')},
    {id : 11, name : '', sourceIcon: require('../../assets/icons/running.png')},
    {id : 12, name : '', sourceIcon: require('../../assets/icons/play-button.png')},
    {id : 13, name : '', sourceIcon: require('../../assets/icons/open-magazine.png')},
    {id : 14, name : '', sourceIcon: require('../../assets/icons/zoo.png')},
];

/**
1. Boire un café/the au bord d'une terasse
2. Faire du Vélo  
3. Aller au Parc
4. Aller au Cinéma
5. Aller à une conférence.

6. Aller à un club de santé, un sauna, etc
7. Aller à la librairie ou à la bibliothèque
8. Escalader une montagne / Se promener en montagnes
9.Aller à la plage
10. Visiter une autre ville/Departement

11.Aller faire du ski alpin ou du ski de fond.
12.Courrir, faire de la gymnastique, des exercices physiques, etc
13.Créer ou arranger des chansons ou de la musique.
14.Lecture en plein air
15.Zoo
 */
const activitesWithWeight = [
    {  id : 0, 
       name : 'Faire du Vélo',
       saison : ['s'],
       fact : 0.17
    },
    {  id : 1, 
       name : `Boire un café/the au bord d'une terasse`,
       saison : ['s'],
       fact : 0.13
    },
    {  id : 2, 
       name : `Aller au Parc`,
       saison : ['s'],
       fact : 0.10
    },
    {  id : 3, 
       name : `Aller au Cinéma`,
       saison : ['s','n'],
       fact : 0.14
    },
    {  id : 4, 
       name : `Aller à une conférence.`,
       saison : ['s'],
       fact : 0.15
    },
   {  id : 5, 
       name : `Aller à un club de santé, un sauna, etc`,
       saison : ['s','n'],
       fact : 0.07
    },
   {  id : 6, 
       name : `Aller à la librairie ou à la bibliothèque`,
       saison : ['s','n'],
       fact : 0.05
    },
   {  id : 7, 
       name : `Escalader une montagne / Se promener en montagnes`,
       saison : ['s','n'],
       fact : 0.01
    },
   {  id : 8, 
       name : `Aller à la plage`,
       saison : ['s'],
       fact : 0.18
    },
   {  id : 9, 
       name : `Visiter une ville/village`,
       saison : ['s','n'],
       fact : 0.08
    },
   {  id : 10, 
       name : `Aller faire du ski alpin ou du ski de fond.`,
       saison : ['n'],
       fact : 0.03
    },
   {  id : 11, 
       name : `Courrir, faire de la gymnastique, des exercices physiques, etc`,
       saison : ['s'],
       fact : 0.18
    },
   {  id : 12, 
       name : `Créer ou arranger des chansons ou de la musique.`,
       saison : ['s','n'],
       fact : 0.05
    },
   {  id : 13, 
       name : `Lecture en plein air`,
       saison : ['s'],
       fact : 0.17
    },
   {  id : 14, 
       name : `Zoo`,
       saison : ['s','n'],
       fact : 0.11
    }
  ];





const evaluteTemp = function(temp, saison, fact){
    let weight =0;
   if(temp <=0){
      if(saison.includes('n')){
        weight = (50 + Math.abs(temp) * Math.floor(Math.random()*3))* (1 + fact)
      }else{
        weight = (50 - Math.abs(temp) * Math.floor(Math.random()*6))* (1 + fact)
      }
    }else{
      if( temp > 0 &&  temp < 10){
        if(saison.includes('n')){
            weight = (40 + Math.abs(temp) * Math.floor(Math.random()*3))* (1 + fact)
        }else{
            weight = (50 - (Math.abs(temp) * Math.floor(Math.random()*3)))* (1 + fact)
        }
        
      }else if( temp >= 10 &&  temp < 20){
        if(saison.includes('n')){
            weight = (20 + Math.abs(temp) * Math.floor(Math.random()*3))* (1 + fact)
        }else{
            weight = (50 + (Math.abs(temp)) * Math.floor(Math.random()*3))* (1 + fact)
        }
        
      }else if( temp >= 20 &&  temp < 30){
        if(saison.includes('n')){
            weight = (30 + Math.abs(temp) * Math.floor(Math.random()*3))* (1 + fact)
        }else{
            weight = (40 + (Math.abs(temp))* Math.floor(Math.random()*4))* (1 + fact)
        }
        
      } else if( temp >= 30){
        
        if(saison.includes('n')){
            weight = (40 + Math.abs(temp) * Math.floor(Math.random()*3))* (1 + fact)
        }else{
            weight = (30 + (Math.abs(temp)) * Math.floor(Math.random()* 4))* (1 + fact)
        }
    }
    }
    return weight;
};



const evaluteWind = function(wind, saison, fact){
  let weight = 0;

  if(wind <10){
     if(saison.includes('s')){
        weight = (30 + (Math.abs(wind)) * Math.floor(Math.random()* 3)) * (1 + fact)
     }else {
        weight = (30 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }
    
  }else if( wind >=10 && wind <40){
     if(saison.includes('s')){
        weight = (20 + (Math.abs(wind)) * Math.floor(Math.random()* 3)) * (1 + fact)
     }else {
        weight = (20 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }
    
  }else if( wind >=40 && wind <60){
    if(saison.includes('s')){
        weight = (20 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }else {
        weight = (20 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }
    
  }else {
    if(saison.includes('s')){
        weight = (10 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }else {
        weight = (10 + (Math.abs(wind)) * Math.floor(Math.random()* 2)) * (1 + fact)
     }
  }
  return weight;
};

const evaluteRain = function(rain, saison){
  let weight = 0;
  if( rain < 0.001 ){
    if(saison.includes('s')){
        weight = 70 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }else {
        weight = 40 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }
    
  } else if (rain >= 0.001  && rain < 0.25 ){
    if(saison.includes('s')){
        weight = 60 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }else {
        weight = 30 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }
    
  } else if (rain >= 0.25  && rain < 0.5 ){
    if(saison.includes('s')){
        weight = 40 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }else {
        weight = 20 + (Math.abs(rain)) * Math.floor(Math.random()* 20)
     }
    
  } else if (rain >= 0.5 && rain < 1.0 ){
    if(saison.includes('s')){
        weight = 20 + (Math.abs(rain)) * Math.floor(Math.random()* 30)
     }else {
        weight = 10 + (Math.abs(rain)) * Math.floor(Math.random()* 30)
     }
    
  } else{
    if(saison.includes('s')){
        weight = 10 + (Math.abs(rain)) * Math.floor(Math.random()* 30)
     }else {
        weight = 5 + (Math.abs(rain)) * Math.floor(Math.random()* 30)
     }
  }
  return weight;
};

/*
On va parcourir la liste des activités et pour chaque activité on va affecter un poids : en fonction de la temperature, la precipitation et la vitesse du vent
  puis on va prendre les 4 activités qui ont le max de poids
  
*/
export default getListActivitesWithIconesAndWeight = function(temp, wind, rain){
  let saison = [];
  
  (function(){
    const monthCurrent = new Date().getMonth() + 1;
    if(monthCurrent >=11 || monthCurrent <=3){
      saison.push('n');
    } else if(monthCurrent => 3 && monthCurrent <=4){
      saison.push('s','n');
    }else{
      saison.push('s');
    }
  })()

  return activitesWithWeight.map( el =>{
    return {
        id : el.id,
        name : el.name,
        weighTemp : evaluteTemp(temp, saison, el.fact),
        weighWind : evaluteWind (wind, saison, el.fact),
        weighRain : evaluteRain(rain, saison),
        weight : evaluteTemp(temp, saison, el.fact) + evaluteWind (wind, saison, el.fact) + evaluteRain(rain, saison)
      }
  }).sort((el1, el2) =>{
  if (el1.weight > el2.weight )
     return -1;
  if (el1.weight < el2.weight)
     return 1;
  return 0;
  }).slice(0, 5).map(el =>{
    return {
      id : el.id,
      name : el.name,
      sourceIcon : activitesWithIcons[el.id].sourceIcon
    }
  });

};
