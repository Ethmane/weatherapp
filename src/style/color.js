export const APP_COLORS = {
    darkPrimary: '#4a4f6f',
    primary: '#fff',
    primaryAction: '#aaadfe',
    accent: '#ff5e64',
    primaryText: '#000',
    secondaryText: '#9c9c9c',
    message: '#f5f6fb',  
    lines: '#ececf4',
    lightPrimaryColor: '#f1f1f1',
    category1: '#a629a5',
    category2: '#ed8d80',
    category3: '#c04358',
  };

