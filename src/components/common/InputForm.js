import React from 'react';
import { TextInput, View, Text } from 'react-native';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
const InputForm = ({ label, value, onChangeText, placeholder, secureTextEntry, icon, onBlur}) => {
  const { inputStyle, labelStyle, containerStyle, section, section2, section3 } = styles;
  
        return (
          <View style={containerStyle}>
          <View style={section}>
            <Text style={labelStyle}>{label}</Text>
          </View>
          <View style={section2}>
            <TextInput
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={inputStyle}
            value={value}
            onChangeText={onChangeText}
            autoCapitalize = 'none'
            underlineColorAndroid="rgba(0,0,0,0)"   
            autoCorrect={false}
            onBlur={onBlur}
          /> 
          </View>
          <View style={section3}>
            {icon}
          </View>
          </View>
        )
  
};

const styles = {
  containerStyle: { 
    flex: 1,
    paddingHorizontal: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: APP_COLORS.secondaryText,  
    alignSelf: 'stretch',
    backgroundColor: APP_COLORS.primary,
    flexDirection: 'row',
    alignItems: 'center', 
  },
  section: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'flex-start', 
  },
  section2: {
    flex: 5,
    alignItems: 'center', 
  },
  section3: {
    flex: 2,
    alignItems: 'flex-end', 
  },
  inputStyle: {
    color:  APP_COLORS.primaryText,
    fontFamily: 'IndieFlower',
    fontSize: 16,    
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    width : 200,
    textAlign : 'center'
  },
  labelStyle: {   
    fontSize: 18,
  },
  icone: {   
  },
  
};

export { InputForm };
