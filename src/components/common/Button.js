import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color'; 

const Button = ({ onPress, children, color, disabled, style }) => {
  const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;

  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}
    style={ (disabled) ? [ buttonStyle, {backgroundColor: APP_COLORS.secondaryText }, style ] : [ buttonStyle, {backgroundColor: color || 'gray' }, style ] } >
      <Text style={textStyle} >
        {children}
      </Text>
    </TouchableOpacity>
  );
}

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 16 ,    
    fontWeight: '500',    
    fontFamily: 'IndieFlower',
    color: APP_COLORS.primary
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: APP_COLORS.primary || 'white',
  },

  
};

export {Button};


