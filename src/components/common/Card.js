import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';

const Card = (props) => {
  return (
    <View style={styles.containerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: APP_COLORS.lines || 'gray',
    borderBottomWidth: 0,
    shadowColor:  '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    margin: 10,
    backgroundColor: APP_COLORS.primary || 'white',

  }
};

export {Card};