//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Animated, Easing } from 'react-native';

class RollView extends Component {

    constructor(props) {
        super(props);
        this.state = {
          visible: props.visible,
        };
      };
    
      componentWillMount() {
        this._visibility = new Animated.Value(this.props.visible ? 200 : 0);
      }
    
      componentWillReceiveProps(nextProps) {
        if (nextProps.visible) {
          this.setState({ visible: true });
        }
        Animated.timing(this._visibility, {
          toValue: nextProps.visible ? 200 : 0,
          duration: 1000,
          
        }).start(() => {
          this.setState({ visible: nextProps.visible });
        });
      }
      render() {
        const { visible, style, children, ...rest } = this.props;
    
        const containerStyle = {
             
          transform: [
            { translateY: this._visibility.interpolate({   
                inputRange: [-5, 1],     
                outputRange: [-5, 1],    
              }),
            }
          ],
        };  
    
        const combinedStyle = [containerStyle, style];
        return (
          <Animated.View style={this.state.visible ? combinedStyle : containerStyle} {...rest}>
            {this.state.visible ? children : null}
          </Animated.View>
        );
      }
}

export  {RollView};