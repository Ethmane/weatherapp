import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';


const CardSection = ({children, last}) => {
  return (
    <View style={ (last) ? [styles.containerStyle, {paddingBottom: 10}] : [styles.containerStyle] }>
      {children} 
    </View>
  );
};

const styles = {
  containerStyle: {
    paddingHorizontal: 10,
    paddingTop: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: APP_COLORS.lines || 'gray',
    position: 'relative',
  }
};

export {CardSection};


