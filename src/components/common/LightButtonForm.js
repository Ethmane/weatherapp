import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color'; 

const LightButtonForm = ({ onPress, children, color, disabled, style }) => {
  const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;

  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}
    style={ (disabled) ? [ buttonStyle, {borderColor: APP_COLORS.secondaryText }, style ] : [ buttonStyle, {borderColor: color || 'gray' }, style ] } >
      <Text style={ (disabled) ? [ textStyle, {color: APP_COLORS.secondaryText } ] : [ textStyle, {color:  color || 'gray' } ] } >
        {children}
      </Text>
    </TouchableOpacity>
  );
}

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 16 ,    
    fontWeight: '500',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: 'IndieFlower',
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    alignSelf: 'stretch',
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: APP_COLORS.primary || 'white',
  },

  
};

export {LightButtonForm};


