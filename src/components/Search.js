import React, { Component } from 'react';
import { View, Text, StyleSheet , Image, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Constants, Location, Permissions } from 'expo';// a enlever
import { APP_COLORS } from '../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import List from './List';
import { FadingView, AnimatedSpin, InputForm, Card, CardSection, LightButtonForm } from './common';

class Search extends Component {

    constructor(props){
        super(props)
        this.state = {
            city : null ,
            placeholder: 'Montpellier',
            
            error: '',
            visible: false,

            disabledButton: false,
            loading: false,
            
        }
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'Rechercher une ville',
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,   
                },
            headerTitleStyle: {
                alignSelf: 'center',
                color : APP_COLORS.primary || 'white',
                fontFamily: 'IndieFlower',
            },   
            headerLeft: null,
            headerTintColor: APP_COLORS.primary || 'white'    
        }  
    }  

    componentWillMount() {
        this._getLocationAsync();
    }
   
    _getLocationAsync = async () => {
        
        this.setState({ loading: true, disabledButton: true });
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            error = 'La permission d\'accéder à l\'emplacement a été refusée' ;
            this.toggleError({error});
        }
        let location = await Location.getCurrentPositionAsync({});
        let locationResult = await Location.reverseGeocodeAsync({latitude  : location.coords.latitude, longitude : location.coords.longitude} );
        this.setState({ city: locationResult[0].city, loading: false, disabledButton: false });
    };  
    
    handleValidateForm = () => {

        let { city } = this.state;
        let error = '';
        let regExpression = /^([a-zA-Z\u0080-\u024F]+(?:. |-| |'))*[a-zA-Z\u0080-\u024F]*$/;

        if( city ){
            if( (regExpression.test(city)) ){
                this.props.navigation.navigate('ResultView', {city : this.state.city})
            } else {
                error = 'format de la ville incorrecte' ;
                this.setState({city: ''});
                this.toggleError({error});
            }
        } else {
            error = 'Veuillez taper une ville pour voir un résultat' ;
            this.toggleError({error});
        }
    } 

    toggleError = ({error}) => {
        this.setState({ error, visible: !this.state.visible });
        setTimeout(() => {this.setState({visible: !this.state.visible })}, 3000);
    }
        
    onPressRechercher = () => {
        Keyboard.dismiss();   
        this.handleValidateForm();
    }

    onPressIcone = () => {
        Keyboard.dismiss(); 
        this._getLocationAsync();
    }

    renderIcon = () => {

        if(!this.state.loading){
            return( <Icon name="location" size={25} style={{color: APP_COLORS.accent}} onPress={this.onPressIcone} /> );
        } else {
            return( 
                <AnimatedSpin >
                    <Icon name="spinner-3" size={25} style={{color: APP_COLORS.accent}}  /> 
                </AnimatedSpin>
            );
        }
    }
    
    renderError = () => {  
        const { errorContainer, errorText } = styles;
        const { visible } = this.state;
        
            return ( <FadingView visible={visible} style={errorContainer}>  
                        <Text style={errorText} >
                            {this.state.error}    
                        </Text>
                    </FadingView>  
            );   
    }

    render() {
        const { containerStyle } = styles;

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={containerStyle}>
                    <Card>
                        <CardSection>
                            <InputForm
                            value = {this.state.city}
                            onChangeText = {(text) => this.setState({city : text})}
                            placeholder = {this.state.placeholder}
                            icon={this.renderIcon()}
                            />     
                        </CardSection>
                        <CardSection last={true} >
                            <LightButtonForm 
                            onPress={this.onPressRechercher}
                            color = {APP_COLORS.darkPrimary}
                            disabled={this.state.disabledButton}
                            >   
                            Rechercher
                            </LightButtonForm>
                        </CardSection>
                    </Card>
                    {this.renderError()}     
                </View>
            </TouchableWithoutFeedback> 
        );
    }
}   

const styles = StyleSheet.create({
    containerStyle: {
        flex : 1, 
        backgroundColor: APP_COLORS.lines || 'gray',
        paddingHorizontal: 10,
        paddingTop: 100,
    },
    errorContainer: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 200, 
        marginTop: 40,
        alignSelf: 'center',  
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
    },
    errorText: {
        fontSize: 14,
        color: APP_COLORS.primary || 'white',
        lineHeight: 22,
    },  
    
})

export default StackNavigator ({
    SearchView : {
        screen : Search,
    },
    ResultView : {
        screen : List,
    }
})  

