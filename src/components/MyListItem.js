
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import moment from 'moment';
import 'moment/locale/fr';
import { APP_COLORS } from '../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import { RollView } from './common';
moment.locale('fr');
class MyListItem extends React.PureComponent {

    _onPress = () => {
      this.props.onPressItem(this.props.id);
    };

    formatDate =() => {
        let time = moment(this.props.date).calendar();

        let timeModify;
        if(/AM/.test(time)) {
            
            if (/12:00/.test(time)) {
                timeModify = time.replace("12:00", "minuit");
                timeModify = timeModify.replace("AM", "");
            } else {
                timeModify = time.replace("AM", "");
            }
        } else  {
            
            if (/3:00/.test(time)) {
                timeModify = time.replace("3:00", "15:00");
                timeModify = timeModify.replace("PM", "");
            } else if (/6:00/.test(time)) {
                timeModify = time.replace("6:00", "18:00").replace("PM", "");
            } else if (/9:00/.test(time)) {
                timeModify = time.replace("9:00", "21:00");
                timeModify = timeModify.replace("PM", "");
            } else {
                timeModify = time.replace("PM", "");
            }
        } 
        return (  <Text>{ timeModify }</Text> )
    }
    formatTemperature =() => {
        let temperature = this.props.temperature;
        let temperatureModify;
        const factor = Math.pow(10, 0);
        temperatureModify = Math.round(temperature * factor) / factor;
        return (  <Text>{ temperatureModify }</Text> )
    }
    
    getListOfActivites = ({ temperature }) => {
        const {activityView} = styles;
        if(temperature < 5 ) {
            return (
                <View  style={activityView}>
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/ice-skate.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/ski.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/swimming.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/bowling.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/chess.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/athlete.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/library.png')} />
                </View>
            );  
        } else if (temperature >= 5 && temperature < 15) { 
            return (
                <View  style={activityView}>
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/cinema.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/sauna.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/swimming.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/bowling.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/athlete.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/chess.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/hiking.png')} />
                </View>
            );
        } else if (temperature >= 15 && temperature < 20) { 
            return (
                <View  style={activityView}>
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/climb.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/volleyball.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/bicycle.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/babington.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/football.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/parc.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/roller-skate.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/sailing-boat.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/golf.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/running.png')} />
                </View>
            );
        } else if (temperature >= 20 && temperature < 30) { 
                        return (
                            <View  style={activityView}>
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/climb.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/city-visit.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/volleyball.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/bicycle.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/babington.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/football.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/parc.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/roller-skate.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/sailing-boat.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/golf.png')} />
                                <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/running.png')} />
                            </View>
                        );
        } else { 
            return (
                <View  style={activityView}>
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/sailing-boat.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/swimming.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/beach.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/wind-surf.png')} />
                    <Image style={{width : 40, height : 40 }}  source={require('../../assets/icons/bowling.png')} />
                </View>
            );
        }     
    }
              
    renderActivity = () => {
        let { temperature } = this.props;
        if(this.props.selected) {
            return  this.getListOfActivites({temperature});  
        }
    };   

render() {
        const iconName = this.props.selected ? "chevron-up" : "chevron-down";
        const { text, image, item, firstItem, bigText, itemWeather} = styles;
        let temperature = this.formatTemperature();
        let date = this.formatDate();
        const IMAGES = {  
            '01d' : require('../../assets/weather-icons/01d.png'), // clear sky
            '02d' : require('../../assets/weather-icons/02d.png'), // few clouds
            '03d' : require('../../assets/weather-icons/03d.png'), // scattered clouds
            '04d' : require('../../assets/weather-icons/03d.png'), // broken clouds
            '09d' : require('../../assets/weather-icons/09d.png'), // shower rain
            '10d' : require('../../assets/weather-icons/10d.png'), // rain
            '11d' : require('../../assets/weather-icons/11d.png'), // thunderstorm
            '13d' : require('../../assets/weather-icons/13d.png'), // snow
            '50d' : require('../../assets/weather-icons/50d.png'), // mist
           
            '01n' : require('../../assets/weather-icons/01n.png'), // clear sky
            '02n' : require('../../assets/weather-icons/02n.png'), // few clouds
            '03n' : require('../../assets/weather-icons/03d.png'), // scattered clouds
            '04n' : require('../../assets/weather-icons/03d.png'), // broken clouds
            '09n' : require('../../assets/weather-icons/09d.png'), // shower rain
            '10n' : require('../../assets/weather-icons/10n.png'), // rain
            '11n' : require('../../assets/weather-icons/11d.png'), // thunderstorm
            '13n' : require('../../assets/weather-icons/13d.png'), // snow
            '50n' : require('../../assets/weather-icons/50d.png'), // mist
          }
        
        if(this.props.id === 1){  
            return (
                <View  style={firstItem}>
                        <View  style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <View style={{flex: 2}} >
                                    <Text style={bigText} >
                                    {temperature} c°
                                    </Text>
                            </View>
                            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}} >
                                    <Image 
                                    style={{width : 50, height : 50 }}
                                    source={IMAGES[this.props.icon]}
                                    />
                                    <Text style={[text, {paddingLeft: 20}]}>{this.props.pressure} hpa</Text>
                                    <Text style={[text, {paddingLeft: 20}]}>{this.props.wind} km/h</Text>
                            </View>
                        </View>
                    {this.getListOfActivites({temperature})}
                </View> 
            );
        } else {
            return (
                <View style={item}>  
                    <View style={itemWeather} >
                        <View style={{flex: 4}} >
                            <Text style={text}> {date}</Text> 
                        </View>
                        <View style={{flex: 2, alignItems: 'center'}}  >
                            <Image 
                            style={{width : 32, height : 32 }}
                            source={IMAGES[this.props.icon]}
                            />
                        </View>
                        <View style={{flex: 2, alignItems: 'flex-end'}} >
                            <Text style={text}>{temperature} C°</Text>
                        </View>
                        <TouchableOpacity onPress={this._onPress} style={{flex: 1, alignItems: 'flex-end'}} >
                            <Icon name={iconName} size={25} style={ {color: APP_COLORS.primary} }  />
                        </TouchableOpacity>
                    </View>
                    {this.renderActivity()}
                </View> 
            );
        }
        
    }
}


export default MyListItem ;  

const styles = StyleSheet.create({
    firstItem: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: APP_COLORS.lines || 'white',
        backgroundColor: 'transparent',
    },
    itemWeather: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    item: {
        paddingLeft: 10,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: APP_COLORS.lines || 'white',
        backgroundColor: 'transparent',
    },
    text: {
        fontSize : 20, 
        color : APP_COLORS.primary || 'white',
        fontWeight: '400',
        fontFamily: 'IndieFlower',
    },
    bigText: {
        fontFamily: 'IndieFlower',
        fontSize : 120, 
        color : APP_COLORS.primary || 'white',
        fontWeight: '300',  
    },
    activityView: {
        marginTop: 25,
        flexDirection: 'row',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    }
       
})
