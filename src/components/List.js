import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, FlatList } from 'react-native';
import axios from 'axios';
import { APP_COLORS } from '../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import {BackgroundImage, Button} from './common';
import MyListItem from './MyListItem';

class List extends Component {

    constructor(props){      
        super(props);
        this.state = {
            city : this.props.navigation.state.params.city,
            data : null,
            nodata: false,
            
            selected: (new Map()) ,
        }
    }  

    componentDidMount() {
        this.fetchWeather();
    }

    static navigationOptions =({navigation}) => {
        return {
            title: `météo ${navigation.state.params.city}`,
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",paddingLeft: 15}} onPress={ () => navigation.navigate('SearchView') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,
                },
            headerTitleStyle: {
                alignSelf: 'center',
                color : APP_COLORS.primary,
                fontFamily: 'IndieFlower',
            },
        }  
    }  

    fetchWeather () {
        let url = `http://api.openweathermap.org/data/2.5/forecast?q=${this.state.city}&mode=json&units=metric&cnt=10&APPID=1a609add64a6c1c810607372b9fc2fe7&lang={fr}`
        axios.get(url).then((response) => {
            let data = response.data.list;
            data.forEach((item, i) => {
                item.key = i + 1;
             });     
            this.setState({data: Array.from(data)})
        }).catch((error) => {
           this.setState({ nodata : true });
        });
    }
    _onPressItem = (key) => {
        this.setState((state) => {
            const selected = new Map(state.selected);
            selected.set(key, !selected.get(key)); 
            return {selected};
        });
    }

    _keyExtractor = (item) => item.key;
    render() {
        const { item, text, image, waitingScreen, container } = styles;
        let source = {uri: 'https://images.unsplash.com/photo-1498793071176-2c542bef8a93?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=fa787207f290053b936aa91cbbf577b4&auto=format&fit=crop&w=668&q=80'};

                if (this.state.data === null ) {
                    if(this.state.nodata === false){
                        return (
                            <View style = {waitingScreen}>
                                <BackgroundImage
                                resizeMode="cover"
                                opacity={0.2}
                                source={source}
                                />
                                <ActivityIndicator
                                size = "large"
                                animating = {true}
                                color = {APP_COLORS.primary || 'white'}
                                >
                                </ActivityIndicator>
                            </View> 
                        )
                    } else {
                        return (
                            <View style = {waitingScreen}>
                                <BackgroundImage
                                resizeMode="cover"
                                opacity={0.2}
                                source={source}
                                />
                                <View style={{ width: '70%', height: 40 }}> 
                                    <Button 
                                    onPress={() => this.props.navigation.navigate('SearchView')}
                                    color = {APP_COLORS.darkPrimary}
                                    disabled={false}
                                    
                                    >   
                                    Returner
                                    </Button>
                                </View>
                            </View> 
                        )
                    }
                } else {
                    
                        return (
                            <View style={container}>
                                <BackgroundImage
                                resizeMode="cover"
                                opacity={0.2}
                                source={source}
                                />
                                    <FlatList
                                    data={this.state.data}
                                    extraData={this.state}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={ ({item}) => ( 
                                        <MyListItem
                                        id={item.key}
                                        onPressItem={this._onPressItem}
                                        selected={!!this.state.selected.get(item.key)}
                                        temperature={item.main.temp}
                                        icon={item.weather[0].icon}
                                        date={item.dt_txt}
                                        windDirection={item.wind.deg}
                                        pressure={item.main.pressure}
                                        wind={item.wind.speed}
                                        precipitation={item.rain}
                                        />
                                        )}
                                    />
                            </View>
                            )
                    
                }
    }
}  

export default List;
 
const styles = StyleSheet.create({
    container: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        flex: 1,  
        paddingHorizontal: 5,
    },
    text: {
        fontSize : 16, 
        color : APP_COLORS.primary || 'white',
    },
    image: {
        width : 50, 
        height : 50,
    },
    waitingScreen: {
        flex : 1, 
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
